# HLin的云音乐小程序

#### 介绍
使用wx开发工具和wx文档仿照网易云音乐做的wx小程序。

##### 主要技术 

##### html 

##### css 

##### javascript 

##### wx组件和API

#### 软件架构
软件架构说明

首页模块： 轮播图，推荐歌单，排行榜

每日推荐模块： 每日推荐歌曲

视频模块： 视频获取，视频播放

个人模块： 登录 ，最近播放

登录模块： 登录

搜索模块:    热点显示，搜索。

歌曲详情模块： 歌曲播放图片，动态效果，上下首切换

![](\hlin_wx_music\展示截图.png)


#### 安装教程

1.  git clone https://gitee.com/liner123/cloud-music-app-of-hlin.git   // 下载代码
2.  cd hlin_music_server  // 进入服务端代码
    这里可能无法启动，需要先执行 npm install 命令安装依赖 （下载速度太慢，可以配置淘宝源见文章https://www.atjiang.com/china-mirrors/）
3.  命令行 输入 npm start 启动服务端代码 (默认端口为 3000)
    这里如果启动失败，看一下控制台信息，显示缺少什么就安装什么 ， npm install xxx
4.  使用微信开发工具打开 hl_wx_music 文件夹

#### 使用说明

1.  如果有帮助到您请点个star ，谢谢 ! ! !
2.  如果有帮助到您请点个star ，谢谢 ! ! !
3.  如果有帮助到您请点个star ，谢谢 ! ! !

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

6.  https://gitee.com/gitee-stars/)
